const fs = require('fs');
(() => {
  const text = fs.readFileSync("./document/index.md", {encoding:'utf-8'});
  const optimizedMarkdown = text.replace(/---[\n\r]+?marp: true[\n\r]+?---[\n\r]+?[\n\r]+?/g,'');
  fs.writeFileSync("./document/index.md" , optimizedMarkdown, {encoding: 'utf-8'})
})();