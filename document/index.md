---
title: JAMstackとPWAで空飛べる
theme: base
style: |
    h1 {
        font-size: 81px;
    }

    h2 {
        font-size: 72px;
    }

    h3 {
        font-size: 54px;
    }

    h4 {
        font-size: 45px;
    }

    p {
        font-size: 36px;
        line-height: 1.8;
    }

    li {
        font-size: 36px;
        line-height: 1.8;
    }

    section {
        background-color: white;
        font-family: "游ゴシック体", YuGothic, "游ゴシック Medium", "Yu Gothic Medium", "游ゴシック", "Yu Gothic", sans-serif;
        color: black;
    }

    p img {
        display: block;
        position: relative;
        left: 0;
        right: 0;
        margin: 20px auto 0;
        border: 1px solid #cccccc;
    }
    section.blue {
        background-color: #0078D7;
        color: white;
    }
---

# JAMstackとPWAで空飛べるかも

[@fluflufluffy](https://twitter.com/fluflufluffy)

![bg cover](./images/bg-1.jpg)

---

## ちまたでうわさのJAMstackとは？

---

![w:70%](./images/img-1.png) 

JavaScript、API、およびプリレンダリングされたマークアップで構成された、サーバレスで提供される高速かつ安全なサイトと動的アプリを作成するモダンなアーキテクチャ

---

- JAMstack
    - JavaScript、APIs、Markup、それらの技術スタックの意
- つまり静的サイトを作る技術の総称
    - JavaScript＋API：動的データコンテンツの取り扱い
    - Markup：事前ビルドによるコンテンツ静的化

---

![w:80%](./images/img-2.jpg)

[https://www.lifeintech.com/2017/12/20/jamstack/より引用](https://www.lifeintech.com/2017/12/20/jamstack/)

---

## JAMstackを構築してみる

---

### 技術選定

大きくわけて以下の3つの技術で構成される：

- Headless CMS（動的なコンテンツデータをAPIとして提供）
- 静的サイトジェネレーター（コンテンツデータとテンプレートからサイトを生成）
- ホスティング（ビルド→静的コンテンツをCDN上にデプロイ）

---

### JAMstackのエコシステム

---

### GasbyjsとNetrify CMSとNetrifyで環境構築（ダイジェスト）

- [GasbyJS](https://www.gatsbyjs.org/tutorial/blog-netlify-cms-tutorial/)
    - 静的サイトジェネレーター
- Netrify
    - ホスティングサービス
- Netrify CMS
    - ヘッドレスCMS

---

### おまじない

[Making a Gatsby Blog with Netlify CMS](https://www.gatsbyjs.org/tutorial/blog-netlify-cms-tutorial/)

```bash
npm i -g gatsby-cli
gatsby new [your-project-name] https://github.com/thomaswangio/gatsby-personal-starter-blog
cd [your-project-name]
```

---

### static/admin/config.ymlの編集

```
backend:
-  name: test-repo
+  name: github
+  repo: your-username/your-repo-name
```

---

### ローカルサーバを起動させる

[loaclhost:8000/admin](localhost:8000/admin)

```
gatsby develop
```

---

### リポジトリを作成する

```bash
git init
git add .
git commit -m "initial commit"
git remote add origin https://github.com/[your-username]/[your-repo-name].git
git push -u origin master
```

---

### Netrify上でぽちぽちボタンを押す

![](./images/img-4.png)

---

![](./images/img-5.png)

---

![](./images/img-6.png)

---

![](./images/img-7.png)

※ [この後GitHub上でOath認証の設定を行いますが割愛](https://www.gatsbyjs.org/tutorial/blog-netlify-cms-tutorial/#step-5)

---

[https://fuwashow-it-test-site.netlify.com/](https://fuwashow-it-test-site.netlify.com/)

![](./images/img-8.png)

---

## PWAとJAMstack

---

### GatbyJSのPWA

- 以下のプラグインを導入することで可能（テンプレートに組み込まれてる場合が多い）
    - [gatsby-plugin-manifest](https://www.npmjs.com/package/gatsby-plugin-manifest)
    - [gatsby-plugin-offline](https://www.npmjs.com/package/gatsby-plugin-offline)
- []

---

## PWAxNetrifyどれくらい早い？（※ 参考程度に）

---

### サーバーから配信

### PWAないバージョン

---

### PWAあるバージョン

---

### キャッシュ問題？

---

## まとめ

---

<!-- _class: blue -->

ありがとうございました！