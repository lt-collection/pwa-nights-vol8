const fs = require('fs');
const path = require('path');
const copyDir = require('copy-dir');
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const targetPath = path.resolve(process.cwd(), 'document');
const distPath = path.resolve(process.cwd(), '.public');

(async () => {
    copyDir.sync(targetPath, distPath, {
      filter: function(stat, filename, filepath) {
        if(/index\.md/.test(filename)) return false;
        return true;
      }
    });

    await imagemin(['.public/images/*'], {
        destination: '.public/hoge',
        plugins: [
            imageminJpegtran(),
            imageminPngquant({
                quality: [0.6, 0.8]
            })
        ]
    });
})();